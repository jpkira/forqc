$(function() {

    var mainConfig = {};
    var socket = io();

    // $('.item-detail').scrollbox();
    
    $('div.navbar-brand').on('click', function(){
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main-content").style.marginLeft = "250px";
    });

    $('a.closebtn').on('click', function(){
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main-content").style.marginLeft = "0";
    });

    $('#createFolder').on('click', function(){
        $.get('/generate', function(data){
            console.log(data);
        })
    });
    
    function loadIncoming() {
        async.eachSeries(mainConfig.incoming, function(item, done) {
            $.get(item.url, function(data){
                var folder = data;
                $('#' + item.id + " .item-count").html(folder.length);
                $('#' + item.id + " .scroll-text").empty();
                
                // data.directories.forEach(function(fld){
                //     var html = "<li>" + fld.name + " - folder count: " + fld.folderCount + "</li>"
                //     $('#' + item.id + " .scroll-text").append(html);
                // });
                
                done();
            });
        }, function(err) {
            if(err) {
                throw err;
            }            
        });
    }

    function loadQCed() {
        async.eachSeries(mainConfig.qced, function(item, done) {            
            $.get(item.url, function(data){
                var folder = data;
                $('#' + item.id + " .item-count").html(folder.length);
                done();
            });

        }, function(err){
            if (err) {
                throw err;
            }
        });
    }

    function loadCapacity() {
        $.get('/capacity', function(data){
            var numberOfFiles = data.length;
            var percentage = (data.length / mainConfig.capacity) * 100;
            

            if(percentage > 100) {
                $(".progress-bar").css("background-color", "#b71c1c");                         
                $('.progress-bar').css('width', '100%');
            } else if (percentage == 100) {                       
                $(".progress-bar").css("background-color", "#c62828");             
                $('.progress-bar').css('width', percentage + '%');
            } else if(percentage >= 90 && percentage < 100) {
                $('.progress-bar').css('width', percentage + '%');
                $(".progress-bar").css("background-color","#d32f2f");  
            } else if(percentage >= 80 && percentage < 90) {
                $('.progress-bar').css('width', percentage + '%');
                $(".progress-bar").css("background-color","#558b2f");              
            } else {
                $('.progress-bar').css('width', percentage + '%');
                // $(".progress-bar").css("background-color","#7cb342");      
            }

            $("#dailyCapacity").html(percentage.toFixed(1) + "% ");
            // $("#totalFiles").html("(" + data + " file/s)");            
        });
    }
   
    function update() {
        $('#clock .time').html(moment().format('h:mm'));
        $('#clock .ampm').html(moment().format('A'));
    }

    setInterval(update, 1000);


    $.get('/config', function(data) {
        mainConfig = data;
        
        loadIncoming();
        loadQCed()
        loadCapacity();
    });


    socket.on('refresh jobs', function(){
        loadIncoming();
        loadQCed();
        loadCapacity();
    });    

});
