$(function(){

    $.get('/logrecords', function(myrecords){
        $('#report-table').dynatable({
            dataset : {
                records: myrecords
            },            

            table: {
                defaultColumnIdStyle: 'trimDash'
            }
        });
    });
    
});