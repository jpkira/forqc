$(function(){
    var picker = new Pikaday({
        field: $('#workdate')[0]
    })    

    $('#foldertype').on('change', function(evt){
        $.get('/config', function(config) {            
            var foldertype = $('#foldertype').find(':selected').text();
            
            $('#categorytype option').remove();

            if(foldertype === 'incoming') {
                $('#categorytype').append('<option>All</option>');
                $.each(config.incoming, function(){
                    $('#categorytype').append('<option>' + this.label + '</option>');
                });
            } else if(foldertype === 'qced') {
                $('#categorytype').append('<option>All</option>');
                $.each(config.qced, function(){
                    $('#categorytype').append('<option>' + this.label + '</option>');
                })
            } else {
                $('#categorytype').append('<option>All</option>');
            }
                    
        });
        
    })
});