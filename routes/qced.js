var express = require('express');
var router = express.Router();
var path = require('path');
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var moment = require('moment');


module.exports = function (config, db) {

    config.qced.forEach(function (item) {
        router.get(item.url, function (req, res, next) {
                        
            var date =  moment().format('YYYYMMDD')  
            var mypath = path.resolve(config.qcedDir, item.path, date);
            
            fs.readdirAsync(mypath).filter(function (filename) {

                filename = path.resolve(mypath, filename);
                return fs.statAsync(filename)
                    .then(function (stat) {
                        return stat.isDirectory();
                    })
                    .catch(function (err) {
                        return false;
                    });
                    
            }).then(function (results) {                
                res.json(results);
            }).catch(function (err) {                
                res.json([]);
            });
            
        });
    });


    return router;
};