var express = require('express');
var router = express.Router();
var path = require('path');
var Promise = require('bluebird');
var fs = require('fs');


router.get('/test', function(req, res, next){
    var mypath = path.resolve('../00_incoming', '20170328');    
    var exists = fs.existsSync(mypath)
    res.send(exists);
});

module.exports = router;