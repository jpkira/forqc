var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var path = require('path');
var jsonfile = require('jsonfile');
var express = require('express');
var router = express.Router();
var moment = require('moment');
var folderSchedule = require('../modules/folderSchedule');

module.exports = function (config, db, folderSchedule) {
  router.get('/', function (req, res, next) {
    res.render('index', {
      headerTitle: config.headerTitle,
      headerSubTitle: config.headerSubTitle,
      incoming: config.incoming,
      qced: config.qced
    });
  });

  router.get('/config', function (req, res, next) {
    res.json(config);
  });

  router.get('/capacity', function (req, res, next) {
    Promise.map(config.qced, function (item) {

      var date = moment().format('YYYYMMDD')
      var mypath = path.resolve(config.qcedDir, item.path, date);

      return fs.readdirAsync(mypath).filter(function (filename) {
        filename = path.resolve(mypath, filename);
        return fs.statAsync(filename)
          .then(function (stat) {
            return stat.isDirectory();
          })
          .catch(function (err) {
            return false;
          });
      }).catch(function(err){
        return new Array();
      })
    }).then(function (results) {      
      var folderArray = Array.prototype.concat.apply([], results)
      res.json(folderArray);
    }).catch(function (err) {
      console.log(err.stack);
      res.json([]);
    })
  })

  router.get('/generate', function(req, res, next){
    folderSchedule.generateFolders();    
    res.send("command executed");
  });

  return router;
};