var express = require('express');
var router = express.Router();
var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var path = require('path');
var moment = require('moment');


/* GET users listing. */
function cleanArray(actual) {
    var newArray = new Array();
    for(var i = 0; i < actual.length; i++) {
      if(actual[i]) {
        newArray.push(actual[i]);
      }
    }

    return newArray;
}

module.exports = function (config) {  
  config.incoming.forEach(function(i) {
    router.get(i.url, function (req, res, next) {

      var mypath = path.resolve(config.incomingDir, i.path);      

      fs.readdirAsync(mypath).filter(function(filename) {
          filename = path.resolve(mypath, filename);          
          return fs.statAsync(filename)
            .then(function(stat){
                return stat.isDirectory();
            })
            .catch(function(err){
                return false;
            })
      }).map(function(directory){
          directory = path.resolve(mypath, directory);
          return fs.readdirAsync(directory).filter(function(filename){
              filename = path.resolve(directory, filename);
              return fs.statAsync(filename)
                .then(function(stat){
                    return stat.isDirectory();
                }).catch(function(err){
                    return false;
                });
          })
      }).then(function(results){
          var folderArray = Array.prototype.concat.apply([], results)          
          res.json(folderArray);
      }).catch(function(err){
          res.json(err.toString());
      });                    
    });
  });

  return router;
};