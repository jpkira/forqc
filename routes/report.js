var express = require('express');
var router = express.Router();
var moment = require('moment');
var excel = require('node-excel-export');


module.exports = function (config, db) {
    router.get('/report', function (req, res, next) {
        res.render('report', {
            headerTitle: config.headerTitle,
            headerSubTitle: config.headerSubTitle
        });
    });

    router.post('/report', function (req, res, next) {
        var searchOptions = {};

        if(req.body.foldertype !== 'All') {
            searchOptions.type = req.body.foldertype;
        }        

        if(req.body.categorytype !== 'All') {
            searchOptions.category = req.body.categorytype;
        }
        
        if(req.body.workdate !== '') {
            searchOptions.createdAt = moment(req.body.workdate).format('YYYYMMDD');        
        }        

        db.find(searchOptions)
            .exec()
            .then(function (docs) {
                var styles = {
                    headerDark: {
                        fill: {
                            fgColor: {
                                rgb: 'FF000000'
                            }
                        },
                        font: {
                            color: {
                                rgb: 'FFFFFFFF'
                            },
                            sz: 14,
                            bold: true,
                            underline: true
                        }
                    },
                    cellPink: {
                        fill: {
                            fgColor: {
                                rgb: 'FFFFCCFF'
                            }
                        }
                    },
                    cellGreen: {
                        fill: {
                            fgColor: {
                                rgb: 'FF00FF00'
                            }
                        }
                    }
                };

                var specification = {
                    _id: { // <- the key should match the actual data key 
                        displayName: 'ID', // <- Here you specify the column header 
                        headerStyle: styles.headerDark, // <- Header style                         
                        width: 120 // <- width in pixels 
                    },
                    folder: {
                        displayName: 'Folder',
                        headerStyle: styles.headerDark,                        
                        width: 120 // <- width in chars (when the number is passed as string) 
                    },
                    type: {
                        displayName: 'Type',
                        headerStyle: styles.headerDark,                        
                        width: 120 // <- width in chars (when the number is passed as string) 
                    },
                    category: {
                        displayName: 'Category',
                        headerStyle: styles.headerDark,                        
                        width: 120 // <- width in chars (when the number is passed as string) 
                    },
                    createdAt: {
                        displayName: 'Created At',
                        headerStyle: styles.headerDark,                        
                        width: 120 // <- width in chars (when the number is passed as string) 
                    },

                    time: {
                        displayName: 'Time',
                        headerStyle: styles.headerDark,                        
                        width: 120 // <- width in chars (when the number is passed as string) 
                    }
                }

                var report = excel.buildExport([
                    {
                        name: 'Qced Report for ' + moment().format('MM-DD-YYYY'),
                        specification: specification,
                        data: docs
                    }
                ]);

                res.attachment('report.xlsx');
                return res.send(report);
            })
            .catch(function (err) {
                console.log(err);                
            });
    });

    return router;
}