var express = require('express');
var router = express.Router();
var path = require('path');


module.exports = function (config, db) {

    router.get('/log', function (req, res, next) {
        res.render('log', {
            headerTitle: config.headerTitle,
            headerSubTitle: config.headerSubTitle     
        });
    });

    router.get('/logrecords', function(req, res, next){
        db.find({})
        .exec()
        .then(function(docs){
            res.json(docs);
        })
        .catch(function(err){
            console.log(err);
        });
    })

    return router;
}