var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var path = require('path');
var jsonFile = require('jsonfile');
var FolderWatch = require('./modules/watcher');


var Datastore = require('nedb-promises');

var app = express();

module.exports = function (mainConfig, io, folderSchedule) {

  this.mainConfig = mainConfig;

  var db = new Datastore({
    filename: path.resolve(mainConfig.logDir, 'log.db'),
    autoload: true
  });

  FolderWatch.setUpIncoming(mainConfig, io, db);
  FolderWatch.setUpQced(mainConfig, io, db);

  var index = require('./routes/index')(mainConfig, db, folderSchedule);
  var incoming = require('./routes/incoming')(mainConfig);
  var qced = require('./routes/qced')(mainConfig, db);
  var log = require('./routes/log')(mainConfig, db);
  var report = require('./routes/report')(mainConfig, db);
  var test = require('./routes/test');
  var Scanner = require('./modules/scanner')(mainConfig, db);

  Scanner.scanIncoming();
  Scanner.scanQced();

  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'pug');

  // uncomment after placing your favicon in /public
  //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: false
  }));
  app.use(cookieParser());
  app.use(express.static(path.join(__dirname, 'public')));

  app.use('/', index);
  app.use('/', incoming);
  app.use('/', qced);
  app.use('/', log);
  app.use('/', report);
  app.use('/', test);

  // catch 404 and forward to error handler
  app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handler
  app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });

  function cleanup() {
    FolderWatch.stop();    
  }

  app.cleanup = cleanup;
  
  return app;
};