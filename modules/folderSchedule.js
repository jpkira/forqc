var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var fss = require('fs');
var path = require('path');
var moment = require('moment');
const log4js = require('log4js');

module.exports = function(config) {

    var logPath = path.resolve(config.logDir, 'folder.log');

    log4js.configure({
        appenders: { 
            folderLog: { type: 'file', filename: logPath },
            console: { type: 'console' }
        },
        categories: {
            folder: { appenders: ['folderLog'], level: 'error' },
            console: { appenders: ['console'], level: 'trace' }, 
            default: { appenders: ['console'], level: 'trace'}
        }
    });

    const errLog = log4js.getLogger('folder');
    const logger = log4js.getLogger('console');
    

    function generateFolders() {
        Promise.map(config.incoming, function(item){
            var date = moment().format('YYYYMMDD');
            var mainPath = path.resolve(config.incomingDir, item.path, date);
            
            if(fss.existsSync(mainPath)) {
                errLog.error(mainPath + ' already exists');
                return Promise.resolve(mainPath);
            }
            
            return fs.mkdirAsync(mainPath);
        }).then(function(result){
            logger.info(moment().format('YYYYMMDD') + " Incoming Folders created");
        }).catch(function(err){
            errLog.error(err.stack);
        });

        Promise.map(config.qced, function(item){
            var date = moment().format('YYYYMMDD');
            var mainPath = path.resolve(config.qcedDir, item.path, date);
            
            if(fss.existsSync(mainPath)) {
                errLog.error(mainPath + ' already exists');
                return Promise.resolve(mainPath);
            }
            
            return fs.mkdirAsync(mainPath);
        }).then(function(result){
            logger.info(moment().format('YYYYMMDD') + " QCed Folders created");
        }).catch(function(err){
            errLog.error(err.stack);
        });
    }


    var folderSchedule = {
        generateFolders: generateFolders
    };

    return folderSchedule;
};