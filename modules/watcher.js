var Promise = require('bluebird');
var path = require('path');
var chokidar = require('chokidar');
var async = require('async');
var moment = require('moment');


var log = console.log.bind(console);
var watcherIncoming;
var watcherQced;

var incomingArray = [];
var qcedArray = [];

var FolderWatch = {
    setUpIncoming: function (config, io, db) {

        var parentDirs = [];

        config.incoming.forEach(function (i) {
            parentDirs.push(i.path);
        });

        Promise.map(config.incoming, function (item, index, len) {
            var watcher = chokidar.watch('dir', {
                persistent: true,
                depth: 1,
                usePolling: true,
                interval: 500,
                ignoreInitial: true
            });

            watcher.on('addDir', function (fullPath) {

                    var parentDir = path.dirname(fullPath).split(path.sep).pop();

                    if (!parentDirs.includes(parentDir)) {

                        var fldArray = fullPath.split(path.sep);
                        var mainFolder = fldArray.pop();
                        var dateFolder = fldArray.pop();

                        var newDoc = {
                            createdAt: dateFolder,
                            type: 'incoming',
                            category: item.label,
                            folder: mainFolder
                        }

                        db.count(newDoc)
                            .then(function (count) {
                                newDoc.time = moment().format('h:mm:ss a');
                                if (count === 0) {
                                    return db.insert(newDoc);
                                } else {
                                    return Promise.resolve(newDoc);
                                }
                            })
                            .then(function (doc) {
                                log('Directory', fullPath, 'has been added');
                                io.emit('refresh jobs');
                            }).catch(function (err) {
                                log(err);
                            });
                    }
                })
                .on('unlinkDir', function (fullPath) {
                    log('Directory', fullPath, 'has been removed');
                    io.emit('refresh jobs');
                })
                .on('error', function (err) {
                    log('Error happened', error);
                });

            var mainPath = path.resolve(config.incomingDir, item.path);
            log(mainPath);
            watcher.add(mainPath);

            return watcher;
        }).then(function (results) {
            Array.prototype.concat(incomingArray, results);
        })
    },

    setUpQced: function (config, io, db) {
        var parentDirs = [];

        config.qced.forEach(function (i) {
            parentDirs.push(i.path);
        });

        Promise.map(config.qced, function (item, index, len) {
            var watcher = chokidar.watch('dir', {
                persistent: true,
                depth: 1,
                usePolling: true,
                interval: 500,
                ignoreInitial: true
            });

            watcher.on('addDir', function (fullPath) {

                    var parentDir = path.dirname(fullPath).split(path.sep).pop();

                    if (!parentDirs.includes(parentDir)) {

                        var fldArray = fullPath.split(path.sep);
                        var mainFolder = fldArray.pop();
                        var dateFolder = fldArray.pop();

                        var newDoc = {
                            createdAt: dateFolder,
                            type: 'qced',
                            category: item.label,
                            folder: mainFolder
                        };

                        db.count(newDoc)
                            .then(function (count) {
                                newDoc.time = moment().format('h:mm:ss a');
                                if (count === 0) {
                                    return db.insert(newDoc)
                                } else {
                                    return Promise.resolve(newDoc);
                                }
                            })
                            .then(function (doc) {
                                log('Directory', fullPath, 'has been added');
                                io.emit('refresh jobs');
                            }).catch(function (err) {
                                log(err);
                            });
                    }
                })
                .on('unlinkDir', function (fullPath) {
                    log('Directory', fullPath, 'has been removed');
                    io.emit('refresh jobs');
                })
                .on('error', function (err) {
                    log('Error happened', error);
                });

            var mainPath = path.resolve(config.qcedDir, item.path);
            log(mainPath);
            watcher.add(mainPath);

            return watcher;
        }).then(function (results) {
            Array.prototype.concat(qcedArray, results);
        })
    },

    stop: function () {
        incomingArray.forEach(function (w) {
            w.close();
        });

        incomingArray.forEach(function (w) {
            w.close();
        });
    }
};

module.exports = FolderWatch;