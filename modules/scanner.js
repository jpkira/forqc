var Promise = require('bluebird');
var fs = Promise.promisifyAll(require('fs'));
var path = require('path');
var moment = require('moment');

module.exports = function (config, db) {

    function saveToLog(folderArray, configArray, ftype) {
        return Promise.each(folderArray, function (fpath) {
            var segments = fpath.split(path.sep);
            var part1 = segments.pop();
            var part2 = segments.pop();
            var part3 = segments.pop();

            var obj = configArray.find(function (cnf) {
                return cnf.path === part3;
            });

            var doc = {
                createdAt: part2,
                type: ftype,
                category: obj.label,
                folder: part1
            };

            db.count(doc)
                .then(function (count) {
                    if(count === 0) {
                        doc.time = moment().format('h:mm:ss a');
                        db.insert(doc);
                    }
                })
                .catch(function(err){
                    console.log(err);
                });
        });
    }

    var scanner = {
        scanIncoming: function () {
            Promise.map(config.incoming, function (inc) {

                    var level1 = path.resolve(config.incomingDir, inc.path);
                    return level1;

                }).map(function (level1) {
                    return fs.readdirAsync(level1).filter(function (file1) {

                        var level2 = path.resolve(level1, file1);

                        return fs.statAsync(level2).then(function (stat) {
                            return stat.isDirectory();
                        }).catch(function (err) {
                            console.log(err);
                        })
                    }).map(function (item1) {
                        return path.resolve(level1, item1);
                    });
                })
                .then(function (level2) {

                    Promise.map(level2, function (file2) {
                        return Promise.map(file2, function (level3) {
                            return fs.readdirAsync(level3)
                                .filter(function (file3) {

                                    var level4 = path.resolve(level3, file3);

                                    return fs.statAsync(level4).then(function (stat) {
                                        return stat.isDirectory();
                                    }).catch(function (err) {
                                        console.log(err);
                                    })

                                }).map(function (myvalue) {
                                    return path.resolve(level3, myvalue);
                                }).then(function (fldSet) {
                                    return saveToLog(fldSet, config.incoming, 'incoming');
                                });
                        });
                    });
                })
                .catch(function (err) {
                    console.log(err);
                });
        },

        scanQced: function () {
            Promise.map(config.qced, function (q) {

                    var level1 = path.resolve(config.qcedDir, q.path);
                    return level1;

                }).map(function (level1) {
                    return fs.readdirAsync(level1).filter(function (file1) {

                        var level2 = path.resolve(level1, file1);

                        return fs.statAsync(level2).then(function (stat) {
                            return stat.isDirectory();
                        }).catch(function (err) {
                            console.log(err);
                        })
                    }).map(function (item1) {
                        return path.resolve(level1, item1);
                    });
                })
                .then(function (level2) {

                    Promise.map(level2, function (file2) {
                        return Promise.map(file2, function (level3) {
                            return fs.readdirAsync(level3)
                                .filter(function (file3) {

                                    var level4 = path.resolve(level3, file3);

                                    return fs.statAsync(level4).then(function (stat) {
                                        return stat.isDirectory();
                                    }).catch(function (err) {
                                        console.log(err);
                                    });

                                }).map(function (myvalue) {
                                    return path.resolve(level3, myvalue);
                                }).then(function (fldSet) {
                                    saveToLog(fldSet, config.qced, 'qced');
                                });
                        });
                    });
                })
                .catch(function (err) {
                    console.log(err);
                });
        }
    };

    return scanner;
};